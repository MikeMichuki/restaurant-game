package game;

public class Restaurant {
	int reputation;
	public void setReputation(int reputation) {	
		this.reputation = reputation;
	}

	public void recordClientSatisfactionOnFood() {
		reputation++;
	}

	public void recordClientSatisfactionOnBeverage() {
		reputation++;
	}

	public void recordClientSatisfactionOnService() {
		reputation++;
	}

	public void recordClientDissatisfactionOnFood() {
		reputation--;
	}

	public void recordClientDissatisfactionOnBeverage() {
		reputation--;
	}

	public void recordClientDissatisfactionOnService() {
		reputation--;
	}

	
	public void updateReputation() {
	}

	public int getReputation() {
		return reputation;
	}
}

package game;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class GameSteps {
	Restaurant restaurant = null;
	
	@Given("^the restaurant reputation is (\\d+)$")
	public void the_restaurant_reputation_is(int reputation) throws Throwable {
		restaurant = new Restaurant();
		restaurant.setReputation(reputation);
	}

	@Given("^(\\d+) clients are (\\w*)satisfied with the (\\w+)$")
	public void clients_are_satisfied_with_the_food(int numberOfClients, String prefix, String matter) throws Throwable {
		matter = matter.toLowerCase();
		for (int i = 0; i < numberOfClients; i++) {
			if (prefix.isEmpty()) {
				if (matter.startsWith("food"))
					restaurant.recordClientSatisfactionOnFood();
				else if (matter.startsWith("beverage"))
					restaurant.recordClientSatisfactionOnBeverage();
				else
					restaurant.recordClientSatisfactionOnService();
			} else {
				if (matter.startsWith("food"))
					restaurant.recordClientDissatisfactionOnFood();
				else if (matter.startsWith("beverage"))
					restaurant.recordClientDissatisfactionOnBeverage();
				else
					restaurant.recordClientDissatisfactionOnService();

			}
				
		}
	}

	@When("^the restaurant reputation is updated$")
	public void the_restaurant_reputation_is_updated() throws Throwable {
		restaurant.updateReputation();
	}

	@Then("^the restaurant reputation becomes (\\d+)$")
	public void the_restaurant_reputation_becomes(int reputation) throws Throwable {
		Assert.assertEquals(reputation, restaurant.getReputation());
	}
	
	@Given("^The game has started$")
	public void The_game_has_started() throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}

	@Given("^The restaurant is created$")
	public void The_restaurant_is_created() throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}

	@Given("^The restaurant reputation is (\\d+)$")
	public void The_restaurant_reputation_is(int arg1) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}

	@Given("^(\\d+) clients are \"([^\"]*)\" with the \"([^\"]*)\"$")
	public void clients_are_with_the(int arg1, String arg2, String arg3) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}

	@When("^The restaurant reputation is updated$")
	public void The_restaurant_reputation_is_updated() throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}

	@Then("^The restaurant reputation should be (\\d+)$")
	public void The_restaurant_reputation_should_be(int arg1) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}
}
